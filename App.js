import { NavigationContainer } from '@react-navigation/native';
import SigninScreen from './src/screens/signin';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useEffect } from 'react';
import { setupDB } from './src/models/db';
import HomeScreen from './src/screens/home';
import HistoryScreen from './src/screens/history';
import ProfileScreen from './src/screens/profile';
import { StatusBar } from 'expo-status-bar';

const Stack = createNativeStackNavigator();

/*
  Huvudkomponent
*/
export default function App() {

  useEffect(() => {
    
    // skapa databas
    setupDB();
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Signin">
        <Stack.Screen name="Signin" component={SigninScreen} options={{ title: 'Signin' }} />
        <Stack.Screen name="Home" component={HomeScreen} options={{ title: 'Home' }} />
        <Stack.Screen name="History" component={HistoryScreen} options={{ title: 'History' }} />
        <Stack.Screen name="Profile" component={ProfileScreen} options={{ title: 'Profile' }} />
      </Stack.Navigator>
      <StatusBar />
    </NavigationContainer>
  );
}
