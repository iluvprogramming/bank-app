import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase("database.db", "0.2");

// denna funktion omsluter "executeSql" i ett löfte
const asyncTransaction = (sqlStatement, args = []) => {
  return new Promise(async (resolve, reject) => {
    db.transaction(tx => {
      tx.executeSql(
        sqlStatement,
        args,
        (transaction, resultSet) => {resolve([transaction, resultSet])},
        (transaction, error) => {reject([transaction, error])}
        )
    })
  })
}

// // sätta upp databasen. måste köras i början av applikationen
export const setupDB = () => {
  db.transaction(tx => {
    console.log("INSTÄLLNING AV DATABAS....")
    // skapar användartabell
    tx.executeSql('CREATE TABLE IF NOT EXISTS User (id INTEGER PRIMARY KEY AUTOINCREMENT, username TEXT, password TEXT, balance INTEGER);')
    // skapar historiktabell
    tx.executeSql('CREATE TABLE IF NOT EXISTS History (id INTEGER PRIMARY KEY AUTOINCREMENT, amount INTEGER, type TEXT, createdAt DATETIME DEFAULT CURRENT_TIMESTAMP);')
    // lägger till användare om användaren inte finns
    tx.executeSql('SELECT * FROM User;',[],
    (_, { rows: { _array } }) => {
      if(_array.length === 0) {
        tx.executeSql('INSERT INTO User (username, password, balance) values ("admin", "password", 4000);')
      }
    })
  })
}

// // hämta användardata från databasen
export const getUserData = async () => {
  try {
    const [_, data] = await asyncTransaction('SELECT * FROM User;');
    if (data.rows._array.length > 0) {
      return data.rows._array[0];
    } else {
      return null;
    }
  } catch(e) {
    console.log('ERROR_getUserData', e);
    return null;
  }
}

// ta ut pengar från kontot
export const removeMoney = async (amount) => { //promise boolean
  try {
    const user = await getUserData();
    await asyncTransaction('UPDATE User SET balance = ? WHERE id = ?;', [user.balance - amount, user.id]);
    await asyncTransaction('INSERT INTO History (amount, type) VALUES (?, ?);', [amount, 'WITHDRAW']);
    return true;
  } catch(e) {
    console.log('ERROR_removeMoney', e);
    return false;
  }
}

// sätt in pengar på kontot
export const addMoney = async (amount) => {
  try {
    const user = await getUserData();
    await asyncTransaction('UPDATE User SET balance = ? WHERE id = ?;', [user.balance + amount, user.id]);
    await asyncTransaction('INSERT INTO History (amount, type) VALUES (?, ?);', [amount, 'DEPOSIT']);
    return true;
  } catch(e) {
    console.log('ERROR_addMoney', e);
    return false;
  }
}

// få uttags- och insättningshistorik
export const getHistory = async () => {
  try {
    const [_, data] = await asyncTransaction('SELECT * FROM History ORDER BY createdAt DESC;');
    return data.rows._array;
  } catch(e) {
    console.log('ERROR_getHistory', e);
    return null;
  }
}

// uppdaterar användarens användarnamn
export const updateUsername = async (username) => {
  try {
    await asyncTransaction('UPDATE User SET username = ?;', [username]);
    return true;
  } catch(e) {
    console.log('ERROR_updateUsername', e);
    return false;
  }
}

// uppdaterar användarens lösenord
export const updatePassword = async (password) => {
  try {
    await asyncTransaction('UPDATE User SET password = ?;', [password]);
    return true;
  } catch(e) {
    console.log('ERROR_updatePassword', e);
    return false;
  }
}
