import { Button, View, Text, TextInput, StyleSheet, Modal } from 'react-native';
import { useState } from 'react';

/*
  Den här komponenten visar en modell där en användare kan ange ett belopp att sätta in.
*/
export default function DepositModal({ open, onClose, onDeposit }) {
  // status för inmatning av belopp
  const [amount, setAmount] = useState('');
  // status för felmeddelande
  const [error, setError] = useState(null);

  const handleDeposit = () => {
    // gör ingenting när beloppet inte är definierat
    if (amount.trim() === '') {
      setError('Ange ett siffra!');
      return;
    }       

    // // tar bort alla icke-numeriska tecken och konverterar dem från sträng till int
    const amountInt = Math.abs(parseInt(amount.replace(/\D/g,'')))

    onDeposit(amountInt);
    handleClose();
  }

  const handleClose = () => {
    setAmount('');
    onClose();
  }

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={open}
      onRequestClose={onClose} 
    >
      <View style={styles.container}>
        <View style={styles.modal}>
          <View>
            <Text>Sätta in pengar</Text>
            <TextInput keyboardType="numeric" style={styles.input} value={amount} onChangeText={setAmount} placeholder="Belopp" autoFocus />
          </View>
          <View>
            {error && (
              <Text style={styles.errorText}>{error}</Text>
            )}
            <View style={styles.button}>
              <Button title='Deposition' onPress={handleDeposit} />
            </View>
            <View style={styles.button}>
              <Button title='Avbryt' onPress={handleClose} />
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
}

// komponentstilar
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height:'100%',
    margin: 20,
  },
  modal: {
    backgroundColor: 'white',
    width: '100%',
    padding: 20,
    borderRadius: 12,
    elevation: 3,
    height: 250,
    display: 'flex',
    justifyContent: 'space-between'
  },
  input: {
    borderWidth: 1,
    borderRadius: 4,
    padding: 10,
    marginTop: 6,
    backgroundColor: 'white',
    borderColor: 'lightgray'
  },
  button: {
    marginTop: 12
  },
  errorText: {
    color: 'red',
    marginTop: 6
  }
});