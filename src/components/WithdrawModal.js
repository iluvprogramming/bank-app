import { Button, View, Text, TextInput, StyleSheet, Modal } from 'react-native';
import { useState } from 'react';

/*
  Den här komponenten visar en modal där en användare kan ange ett belopp för att ta ut.
*/
export default function WithdrawModal({ open, onClose, onWithdraw, balance }) {
  // ange för beloppsinmatning
  const [amount, setAmount] = useState('');
  // tillstånd för felmeddelande
  const [error, setError] = useState(null);

  const handleWithdraw = () => {
    // gör ingenting när beloppet inte är definierat
    if (amount.trim() === '') {
      setError('ange ett siffra!');
      return;
    }

    // tar bort alla icke-numeriska tecken och konverterar dem från sträng till int
    const amountInt = Math.abs(parseInt(amount.trim().replace(/\D/g,'')))

    // // om beloppet är högre än det totala saldot, visa ett fel
    if (amountInt > balance) {
      setError('Inte tillräckligt pengar på kontorn!');
      return;
    }

    onWithdraw(amountInt);
    handleClose();
  }

  const handleClose = () => {
    setAmount('');
    onClose();
    setError(null);
  }

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={open}
      onRequestClose={onClose}
    >
      <View style={styles.container}>
        <View style={styles.modal}>
          <View>
            <Text>Widthdraw Money</Text>
            <TextInput keyboardType="numeric" style={styles.input} value={amount} onChangeText={setAmount} placeholder="Amount" autoFocus />
          </View>
          <View>
            {error && (
              <Text style={styles.errorText}>{error}</Text>
            )}
            <View style={styles.button}>
              <Button title='Withdraw' onPress={handleWithdraw} />
            </View>
            <View style={styles.button}>
              <Button title='Cancel' onPress={handleClose} />
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
}

// komponentstilar
const styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height:'100%',
    margin: 20,
  },
  modal: {
    backgroundColor: 'white',
    width: '100%',
    padding: 20,
    borderRadius: 12,
    elevation: 3,
    height: 250,
    display: 'flex',
    justifyContent: 'space-between'
  },
  input: {
    borderWidth: 1,
    borderRadius: 4,
    padding: 10,
    marginTop: 6,
    backgroundColor: 'white',
    borderColor: 'lightgray'
  },
  button: {
    marginTop: 12
  },
  errorText: {
    color: 'red',
    marginTop: 6
  }
});