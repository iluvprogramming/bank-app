import { Button, View, Text, TextInput, StyleSheet } from 'react-native';
import { useState } from 'react';
import { getUserData } from '../models/db';

/*
  Detta är huvudkomponenten för inloggningsskärmen
*/
export default function SigninScreen({ navigation }) {
  // State för inmatning av användarnamn
  const [username, setUsername] = useState('');
  // state för lösenordsinmatning
  const [password, setPassword] = useState('');
  // state för laddningstillstånd
  const [loading, setLoading] = useState(false);
  // status för felmeddelande
  const [error, setError] = useState(null);

  // verifierar användarinmatning
  const checkUserCredentials = async (username, password) => {
    // hämtar användardata
    const user = await getUserData();
    // om användarnamn och lösenord matchar, returnera true
    if (user && user.username === username.toLowerCase() && user.password === password) {
      return true;
    } else {
      return false;
    }
  }

  const handleSignin = async () => {
    setLoading(true);
    // om inloggningsuppgifterna är korrekta, navigera till startsidan.
    if (await checkUserCredentials(username, password)) {
      navigation.replace('Home')
    } else {
      setError('Ogiltigt användarnamn eller lösenord');
      setLoading(false);
    }
  }

  return (
    <View style={styles.constainer}>
      <TextInput style={styles.input} value={username} onChangeText={setUsername} placeholder="anvandärnamn" autoFocus />
      <TextInput style={styles.input} secureTextEntry value={password} onChangeText={setPassword} placeholder="Lösenord" />
      <View style={styles.button}>
        <Button
          disabled={loading}
          style={{ marginTop: 6 }}
          onPress={handleSignin}
          title="signin"
        />
      </View>
      {error && (
        <Text style={styles.errorText}>
          {error}
        </Text>
      )}
    </View>
  );
}

// komponentstilar
const styles = StyleSheet.create({
  constainer: {
    display: 'flex',
    flexDirection: 'column',
    padding: 12
  },
  input: {
    borderWidth: 1,
    borderRadius: 4,
    padding: 10,
    marginTop: 6,
    backgroundColor: 'white',
    borderColor: 'lightgray'
  },
  button: {
    marginTop: 6
  },
  errorText: {
    color: 'red',
    marginTop: 6
  }
});