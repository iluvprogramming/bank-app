import { View, StyleSheet, ScrollView } from 'react-native';
import { useState, useEffect } from 'react';
import { getHistory } from '../models/db';
import { Table } from 'react-native-table-component';
import { Row } from 'react-native-table-component';
import { Rows } from 'react-native-table-component';

/*
  Detta är huvudkomponenten för History Screen
*/
export default function HistoryScreen() {
  // data för tabellhuvud
  const [tableHead] = useState(['Date/Time', 'Type', 'Amount']);//Returns a stateful value, and a function to update it.
  // historikdata
  const [tableData, setTableData] = useState([]);

  useEffect(() => {
    const updateHistory = async () => {
      // hämta historik och spara den i tableData
      const history = await getHistory();
      const rows = [];
      history.map(item => {
        rows.push([item.createdAt, item.type, `${item.type==='DEPOSITION'?'+':'-'} $${item.amount}`]);
      });
      setTableData(rows);
    }
    updateHistory();
  }, []);

  return (
    <ScrollView>
      <View style={styles.constainer}>
        <Table borderStyle={{borderWidth: 2, borderColor: '#ccc'}}>
          <Row data={tableHead} style={styles.head} textStyle={styles.text} />
          <Rows data={tableData} textStyle={styles.text} />
        </Table>
      </View>
    </ScrollView>
  );
}

// komponentstilar
const styles = StyleSheet.create({
  constainer: {
    padding: 12
  },
  head: { height: 40, backgroundColor: '#eee' },
  text: { margin: 6 }
});