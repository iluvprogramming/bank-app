import { View, StyleSheet, Text, TextInput, Button, ToastAndroid } from 'react-native';
import { useEffect, useState } from 'react';
import { getUserData, updatePassword, updateUsername } from '../models/db';

/*
  Detta är huvudkomponenten för profilskärmen
*/
export default function ProfileScreen({ navigation }) {
  // state för inmatning av användarnamn
  const [username, setUsername] = useState('');
  // state för lösenordsinmatning
  const [password, setPassword] = useState('');
  // tillstånd för felmeddelande
  const [error, setError] = useState(null);
  
  useEffect(() => {
    // hämtar användardata
    const updateUserData = async () => {
      const user = await getUserData();
      setUsername(user.username);
      setPassword(user.password);
    }

    updateUserData();
  }, []);

  const handleUpdateUsername = () => {
    if (username.length > 0) {
      // uppdaterar användarnamn
      updateUsername(username);
      setError(null);
      ToastAndroid.show('användarnamn uppdaterat!', ToastAndroid.SHORT);
    } else {
     // show error om användarnamnet har inte angetts av användaren
      setError("var vänlig ange ett användarnamn!");
    }
  }

  const handleUpdatePassword = () => {
    if (password.length > 0) {
      // updating password
      updatePassword(password);
      setError(null);
      ToastAndroid.show('lösenord uppdaterat!', ToastAndroid.SHORT);
    } else {
      // visa fel är lösenordet inte har angetts av användaren
      setError("Vänligen ange ett lösenord!");
    }
  }

  return (
    <View style={styles.constainer}>
      <Text>Username:</Text>
      <TextInput style={styles.input} value={username} placeholder="Username" onChangeText={setUsername} />
      <Button title="Update Username" onPress={handleUpdateUsername} />
      <Text style={{marginTop: 12}}>Lösenord:</Text>
      <TextInput style={styles.input} value={password} placeholder="Password" onChangeText={setPassword} />
      <Button title="Update Password" onPress={handleUpdatePassword} />
      {error && (
        <Text style={styles.errorText}>{error}</Text>
      )}
    </View>
  );
}

//komponentstilar
const styles = StyleSheet.create({
  constainer: {
    padding: 12
  },
  input: {
    borderWidth: 1,
    borderRadius: 4,
    padding: 10,
    marginTop: 6,
    backgroundColor: 'white',
    borderColor: 'lightgray',
    marginBottom: 6,
  },
  button: {
    marginTop: 12
  },
  errorText: {
    color: 'red',
    marginTop: 6
  }
});