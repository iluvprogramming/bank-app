import { View, Text, StyleSheet, TouchableOpacity, Pressable } from 'react-native';
import { useState, useEffect } from 'react';
import WithdrawModal from '../components/WithdrawModal';
import DepositModal from '../components/DepositModal';
import { addMoney, getUserData, removeMoney } from '../models/db';

/*
  Detta är huvudkomponenten för hemskärmen
*/
export default function HomeScreen({ navigation }) {
  //staten att hantera återkallande modal uttag
  const [openWithdrawModal, setOpenWithdrawModal] = useState(false)
  // staten för att hantera insättningsmodal
  const [openDepositModal, setOpenDepositModal] = useState(false)

  // tillstånd för att spara totalt saldo för användaren
  const [balance, setBalance] = useState(0);

  useEffect(() => {
    // hämtar användarens totala saldo
    const updateBalance = async () => {
      const user = await getUserData();
      setBalance(user.balance);
    }
    updateBalance();
  }, []);

  // minska pengar från totala saldot
  const withdrawMoney = (amount) => {
    removeMoney(amount)
    setBalance(state => state - amount);
  }

  // lägg till pengar från det totala saldot
  const depositMoney = (amount) => {
    addMoney(amount)
    setBalance(state => state + amount);
  }

  // flytta till historiksidan
  const openHistory = () => {
    navigation.push('History');
  }

  // flytta till profilsidan// flytta till profilsidan
  const openProfile = () => {
    navigation.push('Profile');
  }

  // flytta till inloggningssidan
  const handleLogout = () => {
    navigation.replace('Signin');
  }

  return (
    <View style={styles.constainer}>
      <View style={{ display: 'flex', flexDirection: 'row', justifyContent: 'space-between' }}>
        <Text>Your balance:</Text>
        <Pressable onPress={handleLogout}>
          <Text>Logga ut</Text>
        </Pressable>
      </View>
      <View style={styles.balanceArea}>
        <Text style={styles.balanceText}>${balance}</Text>
      </View>
      <View style={styles.row}>
        <TouchableOpacity style={styles.button} onPress={() => setOpenWithdrawModal(true)}>
          <Text style={styles.buttonText}>Uttag</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={() => setOpenDepositModal(true)}>
          <Text style={styles.buttonText}>Deposition</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.row}>
        <TouchableOpacity style={styles.button} onPress={openHistory}>
          <Text style={styles.buttonText}>Historik</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.button} onPress={openProfile}>
          <Text style={styles.buttonText}>Redigera profil</Text>
        </TouchableOpacity>
      </View>
      <WithdrawModal
        open={openWithdrawModal}
        onClose={() => setOpenWithdrawModal(false)}
        onWithdraw={withdrawMoney}
        balance={balance}
      />
      <DepositModal
        open={openDepositModal}
        onClose={() => setOpenDepositModal(false)}
        onDeposit={depositMoney}
      />
    </View>
  );
}

// komponentstilar
const styles = StyleSheet.create({
  constainer: {
    padding: 12
  },
  balanceArea: {
    backgroundColor: 'white',
    borderRadius: 16,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 16,
    height: 80,
    marginTop: 8,
    marginBottom: 16,
  },
  balanceText: {
    fontWeight: '700',
    fontSize: 30
  },
  row: {
    display: 'flex',
    flexDirection: 'row'
  },
  button: {
    flex: 1,
    backgroundColor: 'lightgray',
    margin: 6,
    height: 100,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 12
  },
  buttonText: {
    fontWeight: '500',
    fontSize: 16
  },
});