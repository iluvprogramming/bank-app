import { registerRootComponent } from 'expo';

import App from './App';

// registerRootComponent anropar AppRegistry.registerComponent('main', () => App);
// Det säkerställer också att oavsett om du laddar appen i Expo Go eller i en inbyggd version,
// miljön är korrekt inställd.
registerRootComponent(App);
